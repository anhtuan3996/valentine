<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Elasticquent\ElasticquentTrait;

class Product extends Model
{
//    use ElasticquentTrait;



    protected $table = 'product';
    protected $fillable = ['id','image', 'quantity', 'price', 'name', 'desc', 'category_id', 'brand_id', 'created_at', 'updated_at', 'used'];
    public $timestamps = true;

}
