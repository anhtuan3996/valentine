<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AddSlideRequest;
use Illuminate\Http\Request;
use App\Slide;
use App\Http\Controllers\Controller;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;
class SlideController extends Controller
{
    public function index()
    {
            $slides = Slide::select('id','image')
                ->orderBy('id', 'DESC')
                ->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('admin.slide.index', ['slides' =>$slides]);
    }

    public function form_add()
    {
        return view('admin.slide.add');
    }

    public function add(AddSlideRequest $request)
    {
        $fileName = Uuid::generate().'.'.pathinfo($request->file('image-service')->getClientOriginalName(), PATHINFO_EXTENSION);
        $request->file('image-service')->storeAs('public/slide', $fileName);
        Slide::insert(
            [
                'image' => $fileName,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        );

        return redirect(route('slide.index'))
            ->with('alert-success', trans('messages.successfully_created', ['name' => 'slide']));
    }

    public function image($id)
    {
        $photo = Slide::where('id', trim($id))
            ->first();

        if (empty($photo)) {
            return abort(404);
        }
        $path = storage_path().'/app/public/slide/'.$photo->image;
        echo file_get_contents($path);
        return true;

    }

}
