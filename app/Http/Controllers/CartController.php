<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartUpdateRequest;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\OrderDetail;
use App\Product;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{

    public function cart($id)
    {
            $product = Product::select('id', 'quantity', 'name','price')
                ->where('id', trim($id))
                ->firstOrFail();

        $imageProduct = route('product.image',$product->id);
        Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $product->price, 'options' => ['image' => $imageProduct]]);
        return redirect()->back()->with('alert-success', trans('messages.add_cart'));
    }

    public function all_cart()
    {
        $carts = Cart::content();

        return view('front-end.cart', ['carts' => $carts]);
    }
    public function delete($rowId)
    {
        Cart::remove($rowId);
        return redirect()->back()->with('alert-success', trans('messages.delete_cart'));

    }
    public function update($rowId, $id, CartUpdateRequest $request) {
        $product = Product::select('id', 'quantity', 'used', 'name')
            ->where('id', trim($id))
            ->firstOrFail();
        if ($product->used - $product->quantity >= 0 || $product->quantity < $request->input('quantity')) {
            return redirect()->back()->with('alert-warning', 'Sản phẩm '.$product->name.' vượt quá giới hạn, hoặc đã hết hàng');
        }

        Cart::update($rowId, $request->input('quantity')); // Will update the quantity
        return redirect()->back()->with('alert-success', trans('messages.update_cart'));
    }

    public function order(OrderRequest $request)
    {

        $carts = Cart::content();
        if(!count($carts) > 0) {
            return redirect()->back()->with('alert-warning', trans('messages.cart_empty'));
        }
        foreach ($carts as $cart) {
            OrderDetail::create([
                'product_id' => $cart->id,
                'total' => $cart->qty
            ]);
            $product = Product::select('id', 'quantity', 'used', 'name')
                ->where('id', trim($cart->id))
                ->firstOrFail();
            if ($product->used - $product->quantity >= 0 || $product->quantity < $cart->qty) {
                return redirect()->back()->with('alert-warning', 'Sản phẩm '.$product->name.' vượt quá giới hạn, hoặc đã hết hàng');
            }
        }

        $order = Order::create([
            'name' =>$request->input('name'),
            'address' => $request->input('address'),
            'mail' => $request->input('email'),
            'type' => Order::UNPAID,
            'phone' => $request->input('phone'),
            'price' => $request->input('price'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        foreach ($carts as $cart) {
            OrderDetail::create([
                'order_id' =>$order->id,
                'product_id' => $cart->id,
                'total' => $cart->qty,
                'name' => $cart->name,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
            $products = Product::select('id', 'quantity', 'used', 'name')
                ->where('id', trim($cart->id))
                ->firstOrFail();

            $data = [
                'used' => $products->used + $cart->qty,
                'updated_at' => Carbon::now()
            ];
            Product::where('id', trim($cart->id))->update($data);
        }
        Cart::destroy();
        return redirect()->route('frontend.index')->with('alert-success', trans('messages.cart_paid'));

    }


}
