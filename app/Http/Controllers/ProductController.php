<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Language;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index($id = 0)
    {
        $products = Product::select('id', 'quantity', 'image', 'name', 'desc', 'price');
        $categories = Category::select('id', 'name');
        if ($id != 0) {
            $products->where('category_id', '=', trim($id));
        }
        $categories = $categories->get();
        $products = $products->paginate(DEFAULT_PAGINATION_PER_PAGE);
        return view('front-end.gallery', ['products' => $products, 'categories' =>$categories]);
    }


    public function detail($id){
            $product = Product::select('id', 'quantity', 'image', 'name', 'desc', 'price');
        $product = $product->where('id', $id)->firstOrFail();

        return view('front-end.single', ['product' =>$product]);
    }
}
