
@extends('front-end.layouts.app')
@section('content')

    <!-- innerpages_banner -->
    <div class="innerpages_banner">
        <h2>Services</h2>
    </div>
    <!-- //innerpages_banner -->

    <!-- Services -->
    <div class="popular-section-wthree ">
        <div class="container">
            <h3 class="heading">Our Services</h3>
            <div class="service_top_grids">
                <div class="col-md-4 service-grid1">
                    <p><span>Bakery In</span> - euismod vehicula vestibulum. Fusceu llamciorper aliquet dolor id egestas. Nulla leopu rusin, facilisis non cursus ut, egestas sedipsumin. Proin teeuismod vehicula vestibulum. Fusceulla amcor per aliquet dolor id egestas. Nulla leoke purus in, facilisisnon cursus ut, egestas sedin ipsum lorem dolor sit.</p>
                </div>
                <div class="col-md-4 service-grid1">
                    <img src="images/g2.jpg" alt="service_image" />
                </div>
                <div class="col-md-4 service-grid1">
                    <p><span>Bakery In</span> - euismod vehicula vestibulum. Fusceu llamciorper aliquet dolor id egestas. Nulla leopu rusin, facilisis non cursus ut, egestas sedipsumin. Proin teeuismod vehicula vestibulum. Fusceulla amcor per aliquet dolor id egestas. Nulla leoke purus in, facilisisnon cursus ut, egestas sedin ipsum lorem dolor sit.</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="popular-agileinfo">
                <div class="col-md-3 popular-grid">
                    <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                    <h4>Birthday Cakes</h4>
                    <p>Itaque earum rerum hic tenetur a sapiente eds delectus reiciendis maiores hasellus int</p>
                </div>
                <div class="col-md-3 popular-grid">
                    <i class="fa fa-spoon" aria-hidden="true"></i>
                    <h4>Online Sevice</h4>
                    <p>Itaque earum rerum hic tenetur a sapiente eds delectus reiciendis maiores hasellus int</p>
                </div>
                <div class="col-md-3 popular-grid popular-grid-bottom">
                    <i class="fa fa-cutlery" aria-hidden="true"></i>
                    <h4>Delicious Cakes</h4>
                    <p>Itaque earum rerum hic tenetur a sapiente eds delectus reiciendis maiores hasellus int</p>
                </div>
                <div class="col-md-3 popular-grid">
                    <i class="fa fa-bed" aria-hidden="true"></i>
                    <h4>Quality Cakes</h4>
                    <p>Itaque earum rerum hic tenetur a sapiente eds delectus reiciendis maiores hasellus int</p>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //Services -->

    <!-- banner-bottom -->
    <div class="col-md-7 bannerbottomleft">

    </div>
    <div class="col-md-5 bannerbottomright banner-bottom mid-section-agileits">
        <h3>Auis Mostrum Rexercita</h3>
        <p>Ut enim ad minima veniam, quis nostrum reiciendis maiores hasellus int
            exercitationem ulla corporis suscipit laboriosam, tenetur a sapiente eds delectus reiciendis maiores
            nisi ut aliquid ex ea.</p>
        <h4><i class="fa fa-delicious" aria-hidden="true"></i>Online Delivery Service</h4>
        <h4><i class="fa fa-crop" aria-hidden="true"></i>Fast And Good Quality</h4>
        <h4><i class="fa fa-birthday-cake" aria-hidden="true"></i>Different Types Of Cakes</h4>
        <h4><i class="fa fa-circle" aria-hidden="true"></i>Quality Staff</h4>
        <h4><i class="fa fa-coffee" aria-hidden="true"></i>Best Service</h4>
    </div>
    <div class="clearfix"></div>
    <!-- //banner-bottom -->

    <!-- stats -->
    <div class="stats" id="stats">
        <div class="container">
            <h3 class="heading">Stats</h3>
            <div class="inner_w3l_agile_grids">
                <div class="col-md-3 w3layouts_stats_left w3_counter_grid">
                    <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                    <p class="counter">45</p>
                    <h3>Branches</h3>
                </div>
                <div class="col-md-3 w3layouts_stats_left w3_counter_grid1">
                    <i class="fa fa-trophy" aria-hidden="true"></i>
                    <p class="counter">165</p>
                    <h3>Awards</h3>
                </div>
                <div class="col-md-3 w3layouts_stats_left w3_counter_grid2">

                    <i class="fa fa-smile-o" aria-hidden="true"></i>
                    <p class="counter">563</p>
                    <h3>Happy Customers</h3>
                </div>
                <div class="col-md-3 w3layouts_stats_left w3_counter_grid3">
                    <i class="fa fa-cogs" aria-hidden="true"></i>
                    <p class="counter">580</p>
                    <h3>Services</h3>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!-- //stats -->


    <!-- subscribe -->
    <div class="subscribe" style="background-color: #ea2035;">
        <div class="container">
            <h3 class="heading">Subscribe To Get Notifications</h3>
            <div class="subscribe-grid">
                <form action="#" method="post">
                    <input type="email" placeholder="Enter Your Email" name="email" required="">
                    <button class="btn1"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
    <!-- //subscribe -->

@endsection