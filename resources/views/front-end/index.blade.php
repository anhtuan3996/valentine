@extends('front-end.layouts.app')

@section('content')
    <!-- banner slider -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
            <li data-target="#myCarousel" data-slide-to="2" class=""></li>
            <li data-target="#myCarousel" data-slide-to="3" class=""></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            @php $index = 1; @endphp
            @forelse($slides as $slide)
                @if($index == 1)
                <div class="item active">
                    <div class="container">
                        <div class="carousel-caption">
                            <div class="col-md-6 slider_left">
                                <h2>facilisis non est ut, <span>bibendum finibus est.</span></h2>
                                <p>Nulla tempus mollis nulla, a sollicitudin orci posuere sit amet. Nam sed consectetur nulla, in auctor nunc.</p>
                                {{--<a href="#about" class="hvr-bounce-to-right read scroll"><span class="fa fa-birthday-cake" aria-hidden="true"></span>Read More</a>--}}
                            </div>
                            <div class="col-md-6 slider_right">
                                <img src="{{asset('storage/slide/'.$slide->image)}}" alt="cake1" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                @else
                    <div class="item item{{$index}}">
                        <div class="container">
                            <div class="carousel-caption">
                                <div class="col-md-6 slider_left">
                                    <h2>facilisis non est ut, <span>bibendum finibus est.</span></h2>
                                    <p>Nulla tempus mollis nulla, a sollicitudin orci posuere sit amet. Nam sed consectetur nulla, in auctor nunc.</p>
                                    {{--<a href="#about" class="hvr-bounce-to-right read scroll"><span class="fa fa-birthday-cake" aria-hidden="true"></span>Read More</a>--}}
                                </div>
                                <div class="col-md-6 slider_right">
                                    <img src="{{asset('storage/slide/'.$slide->image)}}" alt="cake1" />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                @endif
            @empty
                <div class="item active">
                    <div class="container">
                        <div class="carousel-caption">
                            <div class="col-md-6 slider_left">
                                {{--<h2>facilisis non est ut, <span>bibendum finibus est.</span></h2>--}}
                                {{--<p>Nulla tempus mollis nulla, a sollicitudin orci posuere sit amet. Nam sed consectetur nulla, in auctor nunc.</p>--}}
                                {{--<a href="#about" class="hvr-bounce-to-right read scroll"><span class="fa fa-birthday-cake" aria-hidden="true"></span>Read More</a>--}}
                            </div>
                            <div class="col-md-6 slider_right">
                                <img src="{{asset('web/images/cake.png')}}" alt="cake1" />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            @endforelse
            {{--<div class="item item2">--}}
                {{--<div class="container">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<div class="col-md-6 slider_left">--}}
                            {{--<h3>facilisis non est ut, <span>bibendum finibus est.</span></h3>--}}
                            {{--<p>Nulla tempus mollis nulla, a sollicitudin orci posuere sit amet. Nam sed consectetur nulla, in auctor nunc.</p>--}}
                            {{--<a href="#about" class="hvr-bounce-to-right read scroll"><span class="fa fa-birthday-cake" aria-hidden="true"></span>Read More</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 slider_right">--}}
                            {{--<img src="images/cake1.png" alt="cake1" />--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="item item3">--}}
                {{--<div class="container">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<div class="col-md-6 slider_left">--}}
                            {{--<h3>facilisis non est ut, <span>bibendum finibus est.</span></h3>--}}
                            {{--<p>Nulla tempus mollis nulla, a sollicitudin orci posuere sit amet. Nam sed consectetur nulla, in auctor nunc.</p>--}}
                            {{--<a href="#about" class="hvr-bounce-to-right read scroll"><span class="fa fa-birthday-cake" aria-hidden="true"></span>Read More</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 slider_right">--}}
                            {{--<img src="images/cake2.png" alt="cake1" />--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="item item4">--}}
                {{--<div class="container">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<div class="col-md-6 slider_left">--}}
                            {{--<h3>facilisis non est ut, <span>bibendum finibus est.</span></h3>--}}
                            {{--<p>Nulla tempus mollis nulla, a sollicitudin orci posuere sit amet. Nam sed consectetur nulla, in auctor nunc.</p>--}}
                            {{--<a href="#about" class="hvr-bounce-to-right read scroll"><span class="fa fa-birthday-cake" aria-hidden="true"></span>Read More</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 slider_right">--}}
                            {{--<img src="images/cake3.png" alt="cake1" />--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="fa fa-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="fa fa-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!-- The Modal -->
    </div>
    <!-- //banner slider -->

    <!-- welcome -->
    <div class="welcome">
        <div class="container">
            <div class="col-md-6 welcome-w3lright">
                <div class="video-grid-single-page-agileits">
                    <div data-video="qvtjaAW3m-E" id="video"> <img src="{{asset('web/images/about.png')}}" alt="" class="img-responsive" /> </div>
                </div>
            </div>
            <div class="col-md-6 welcome_left">
                <h3 class="agileits-title">Chào mừng đến với cửa hàng valentine</h3>
                <h4>Donec in nisi non ipsum luctus interdi est. Cras ipsum augue, facilisis non estut, bibendum finibus.</h4>
                <p>Phasellus sed semper dolor, sed sodales erat. Donec at mi nunc. Suspendisse dictum lorem nec velit scelerisque, ac egestas sem tempor. Integer at facilisis enim. Vestibulum tristique consequat finibus. Donec ut elementum lorem, id dignissim neque. Curabitur commodo, odio sit amet vestibulum pretium, urna quam tincidunt elit, a tempus ex urna sit amet tortor. Nulla volutpat pulvinar interdum.
                    Mauris vel malesuada magna, in venenatis erat. Vestibulum volutpat faucibus semper.</p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //welcome -->


    <!-- Popular cakes -->
    <div class="popular_cakes">
        <div class="container">
            <h3 class="heading">Sản phẩm</h3>
            <div class="cakes_grids">
                @forelse($products as $product)
                <div class="col-md-4">
                    <div class="cakes_grid1">
                        <img src="{{asset('storage/product/'.$product->image)}}" alt="{{$product->name}}" />
                        <h3><a href="{{route('frontend.product.detail', $product->id)}}">{{$product->name}}</a></h3>
                        <p>{{number_format($product->price,0,",",".")}} vnđ</p>
                        <p><a href="{{route('cart.product', $product->id)}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Thêm vào giỏ hàng</a></p>
                    </div>
                </div>
                @empty
                    <div><h2 class="text-center">Không có dữ liệu</h2></div>
                @endforelse
                    <div class="clearfix"></div>
                    <div class="row text-center">
                        {{ $products->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //Popular cakes -->



    <!-- testimonials -->
    <div class="testimonials">
        <div class="container">
            <h3 class="heading">Ý kiến khách hàng</h3>
            <div class="testimonials-grids">
                <div class="wmuSlider example1 animated wow slideInUp" data-wow-delay=".5s">
                    <div class="wmuSliderWrapper">
                        <article style="position: absolute; width: 100%; opacity: 0;">
                            <div class="banner-wrap">
                                <div class="testimonials-grid">
                                    <div class="testimonials-grid-left">
                                        <img src="{{asset('web/images/t1.jpg')}}" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="testimonials-grid-right">
                                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                            praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias
                                            excepturi sint occaecati cupiditate non provident. Duis maximus, velit posuere rutrum tempus, nibh ligula semper felis.</p>
                                        <h4>Richard Doe</h4>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </article>
                        <article style="position: absolute; width: 100%; opacity: 0;">
                            <div class="banner-wrap">
                                <div class="testimonials-grid">
                                    <div class="testimonials-grid-left">
                                        <img src="{{asset('web/images/t3.jpg')}}" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="testimonials-grid-right">
                                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                            praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias
                                            excepturi sint occaecati cupiditate non provident. Duis maximus, velit posuere rutrum tempus, nibh ligula semper felis.</p>
                                        <h4>Rita James</h4>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </article>
                        <article style="position: absolute; width: 100%; opacity: 0;">
                            <div class="banner-wrap">
                                <div class="testimonials-grid">
                                    <div class="testimonials-grid-left">
                                        <img src="{{asset('web/images/t2.jpg')}}" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="testimonials-grid-right">
                                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                            praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias
                                            excepturi sint occaecati cupiditate non provident. Duis maximus, velit posuere rutrum tempus, nibh ligula semper felis.</p>
                                        <h4>Crisp Ali</h4>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </article>
                        <article style="position: absolute; width: 100%; opacity: 0;">
                            <div class="banner-wrap">
                                <div class="testimonials-grid">
                                    <div class="testimonials-grid-left">
                                        <img src="{{asset('web/images/t1.jpg')}}" alt=" " class="img-responsive" />
                                    </div>
                                    <div class="testimonials-grid-right">
                                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                            praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias
                                            excepturi sint occaecati cupiditate non provident. Duis maximus, velit posuere rutrum tempus, nibh ligula semper felis</p>
                                        <h4>Laura roy</h4>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //testimonials -->

    <!-- subscribe -->
    <div class="subscribe" style="background-color: #ea2035;">
        <div class="container">
            <h3 class="heading">Subscribe To Get Notifications</h3>
            <div class="subscribe-grid">
                <form action="#" method="post">
                    <input type="email" placeholder="Enter Your Email" name="email" required="">
                    <button class="btn1"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
    <!-- //subscribe -->

@endsection