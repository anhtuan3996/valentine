@extends('front-end.layouts.app')
@section('content')

<!-- innerpages_banner -->
<div class="innerpages_banner" style="background: linear-gradient(rgba(23, 22, 23, 0), rgba(23, 22, 23, 0)), url({{asset('web/images/banner1.png')}}) repeat;">
    <h2 style="font-size: 55px;
    font-weight: 600;
    color: #fff;
    text-align: center;
    padding-top: 2em;
    padding-bottom: 2em;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);">Về chúng tôi</h2>
</div>
<!-- //innerpages_banner -->

<!-- about section content -->
<div class="about_section" style="margin-top: 30px;margin-bottom: 30px;">
    <div class="container">
        <div class="col-md-6 about-left">
            <div class="col-md-6 cakeimg">
                <img src="{{asset('web/images/about1.jpg')}}" alt="image" />
                <img src="{{asset('web/images/about2.jpg')}}" alt="image" />
            </div>
            <div class="col-md-6 cakeimg cakeimg2">
                <img src="{{asset('web/images/about3.jpg')}}" alt="image" />
                <img src="{{asset('web/images/cake1.jpg')}}" alt="image" />
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="col-md-6 about_right">
            <h3>Made for you</h3>
            <h3 class="bold">with love</h3>
            <p>Suspendisse vitae vulputate ligula, ac ornare urna. Aenean volutpat, lacus non bibendum ullamcorper, mi neque cursus augue, vel euismod lorem ipsum non eros. Nullam volutpat condimentum pharetra. Etiam eget dapibus dolor. Aenean suscipit nec nisi id dignissim. Nullampos uere quam quis varius rutrum. Cras in egestas mi. Vestibulum odio lorem, lobortis in enim in, vulputate eleifend nisl.</p>
            <a href="#about" class="hvr-bounce-to-right read scroll"><span class="fa fa-birthday-cake" aria-hidden="true"></span>Read More</a>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //about section content -->


<!-- subscribe -->
<div class="subscribe" style="background-color: #ea2035;">
    <div class="container">
        <h3 class="heading">Subscribe To Get Notifications</h3>
        <div class="subscribe-grid">
            <form action="#" method="post">
                <input type="email" placeholder="Enter Your Email" name="email" required="">
                <button class="btn1"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </form>
        </div>
    </div>
</div>
<!-- //subscribe -->


@endsection