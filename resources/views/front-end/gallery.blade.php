
@extends('front-end.layouts.app')

@section('content')
    <!-- innerpages_banner -->
    <div class="innerpages_banner" style="background: linear-gradient(rgba(23, 22, 23, 0), rgba(23, 22, 23, 0)), url({{asset('web/images/banner1.png')}}) repeat;">
        <h2 style="font-size: 55px;
    font-weight: 600;
    color: #fff;
    text-align: center;
    padding-top: 2em;
    padding-bottom: 2em;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);">Sản phẩm</h2>
    </div>
    <!-- //innerpages_banner -->

    <!-- Portfolio section -->
    <section class="portfolio-agileinfo gallery" id="portfolio" style="padding-top: 30px;">
        <div class="container">
            <div class="gallery-grids">
                <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs" role="tablist" data-aos="zoom-in">
                        @foreach($categories as $category)
                        <li role="presentation"><a href="{{route('frontend.product', $category->id)}}" >{{$category->name}}</a></li>
                        @endforeach
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
                            <div class="tab_img">
                                @forelse($products as $product)
                                <div class="col-md-3 col-sm-6 col-xs-6 portfolio-grids" style="margin-bottom: 20px;">
                                    <a href="{{route('frontend.product.detail', $product->id)}}" style="position: relative;">
                                        <img src="{{asset('storage/product/'.$product->image)}}" class="img-responsive" alt="{{$product->name}}" />
                                        <div class="b-wrapper text-center">
                                            <h5 style="color: red;margin-bottom: 5px; margin-top: 5px;">{{$product->name}}</h5>
                                            <p>{{number_format($product->price,0,",",".")}} vnđ</p>
                                        </div>
                                        <p style="position: absolute; top: 50%;left: 20%;"><a href="{{route('cart.product', $product->id)}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>Thêm vào giỏ hàng</a></p>
                                    </a>
                                </div>
                                @empty
                                    <div><h2 class="text-center">Không có sản phẩm nào</h2></div>
                                @endforelse
                                <div class="clearfix"> </div>
                                <div class="row text-center">
                                    {{ $products->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Portfolio section -->

    <!-- subscribe -->
    <div class="subscribe" style="background-color: #ea2035;">
        <div class="container">
            <h3 class="heading">Subscribe To Get Notifications</h3>
            <div class="subscribe-grid">
                <form action="#" method="post">
                    <input type="email" placeholder="Enter Your Email" name="email" required="">
                    <button class="btn1"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                </form>
            </div>
        </div>
    </div>
    <!-- //subscribe -->
@endsection